package com.academiasmoviles.proyecto_final

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.academiasmoviles.proyecto_final.data.AppDataBase
import com.academiasmoviles.proyecto_final.databinding.ActivityDetalleBinding
import com.academiasmoviles.proyecto_final.model.restaurante
import java.util.concurrent.Executors

class DetalleActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetalleBinding
    private lateinit var appDatabase: AppDataBase
    private var codigo = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetalleBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //Action Bar
        val actionbar = supportActionBar
        actionbar!!.title = "Reserva"
        actionbar.setDisplayHomeAsUpEnabled(true)

        appDatabase = AppDataBase.obtenerInstancia(this)

        val bundle = intent.extras

        bundle?.let {

            val restaurante = it.getSerializable("key_restaurante") as restaurante
            binding.tvNombreReserva.text = restaurante.nombre
            binding.tvTipoReserva.text = restaurante.tipo
            binding.edtHorarioReserva.setText(restaurante.atencion)
            codigo = restaurante.codigo

        }

        binding.btnReservar.setOnClickListener {
            startActivity(Intent(this, ListaActivity::class.java))
            val nombre = binding.tvNombreReserva.text.toString()
            val tipo = binding.tvTipoReserva.text.toString()
            val preferencia = binding.edtHorarioReserva.text.toString()
            val restaurante = restaurante(codigo,nombre ,tipo ,preferencia)
            editarReserva(restaurante)
        }

    }
    private fun editarReserva(restaurante: restaurante) {
        Executors.newSingleThreadExecutor().execute {
            appDatabase.restauranteDao().update(restaurante)
            runOnUiThread {
                Toast.makeText(this,"Reserva actualizada",Toast.LENGTH_SHORT).show()
                onBackPressed()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }



}