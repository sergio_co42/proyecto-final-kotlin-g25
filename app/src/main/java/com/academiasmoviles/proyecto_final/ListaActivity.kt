package com.academiasmoviles.proyecto_final

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.academiasmoviles.proyecto_final.data.AppDataBase
import com.academiasmoviles.proyecto_final.databinding.ActivityListaBinding
import com.academiasmoviles.proyecto_final.model.restaurante

class ListaActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListaBinding
    private lateinit var restauranteAdapter: RestauranteAdapter
    private val appDatabase : AppDataBase by lazy{
        AppDataBase.obtenerInstancia(this)
    }
 override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityListaBinding.inflate(layoutInflater)
        setContentView(binding.root)
         //Action Bar
         val actionbar = supportActionBar
         actionbar!!.title = "Restaurantes"
         actionbar.setDisplayHomeAsUpEnabled(false)


        binding.floatingActionButton.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }

        setUpAdapter()
        loadData()
    }

    private fun loadData(){
        appDatabase.restauranteDao().obtenerRestaurante().observe(this, Observer {
            restauranteAdapter.updateList(it)
        })

    }

    private fun setUpAdapter(){
        restauranteAdapter=RestauranteAdapter()
        restauranteAdapter.callbackAgregar = {

            val bundle = Bundle().apply {
                putSerializable("key_restaurante",it)
            }

            val intent = Intent(this,DetalleActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        restauranteAdapter.callbackEditar = {

            val bundle = Bundle().apply {
                putSerializable("key_restaurante",it)
            }

            val intent = Intent(this,DetalleActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }


        binding.rvRestaurante.adapter= restauranteAdapter
        binding.rvRestaurante.layoutManager= LinearLayoutManager(this)
    }
}