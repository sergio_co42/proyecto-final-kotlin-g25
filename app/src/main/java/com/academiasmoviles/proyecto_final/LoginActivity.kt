package com.academiasmoviles.proyecto_final

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.academiasmoviles.proyecto_final.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {
    private val usuario:String ="scaico"
    private val clave:String="1234"
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        recuperarPref()
        //Action Bar
        val actionbar = supportActionBar
        actionbar!!.title = "Login"
        actionbar.setDisplayHomeAsUpEnabled(false)


        binding.tvCrearCuenta.setOnClickListener {
            startActivity(Intent(baseContext,RegistroActivity::class.java))
        }


        binding.btnIngresar.setOnClickListener {

            if (binding.edtUsuario.text.toString()==usuario&&binding.edtClave.text.toString()==clave){
                guardarPref(binding.edtUsuario.text.toString(),binding.edtClave.text.toString())
                startActivity(Intent(baseContext,ListaActivity::class.java))
            }else{
                Toast.makeText(baseContext,"Credenciales Incorrectas", Toast.LENGTH_SHORT).show()
            }

        }

    }


    private fun guardarPref(usuario:String,clave:String){
        getSharedPreferences("Login",0).edit().apply {
            putString("key_usuario",usuario)
            putString("key_clave",clave)
            apply()
        }
    }

    private fun recuperarPref(){
        val preferencia= getSharedPreferences("Login",0)
        binding.edtUsuario.setText(preferencia.getString("key_usuario",""))
        binding.edtClave.setText(preferencia.getString("key_clave",""))
    }
}