package com.academiasmoviles.proyecto_final

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.academiasmoviles.proyecto_final.data.AppDataBase
import com.academiasmoviles.proyecto_final.databinding.ActivityMainBinding
import com.academiasmoviles.proyecto_final.model.restaurante
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var appDataBase: AppDataBase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        appDataBase= AppDataBase.obtenerInstancia(this)
        //Action Bar
        val actionbar = supportActionBar
        actionbar!!.title = "Registro de Restaurante"
        actionbar.setDisplayHomeAsUpEnabled(true)


        binding.btnRegRestaurante.setOnClickListener {
            val nombre= binding.edtNombreRes.text.toString()
            val tipo= binding.edtTipoRes.text.toString()
            val atencion= binding.edtHorarioRes.text.toString()


            val mascota= restaurante(0,nombre,tipo,atencion)
            Executors.newSingleThreadExecutor().execute {
                appDataBase.restauranteDao().insertar(mascota)

                runOnUiThread {
                    Toast.makeText(this,"Restaurante Registrado", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }
            }
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}