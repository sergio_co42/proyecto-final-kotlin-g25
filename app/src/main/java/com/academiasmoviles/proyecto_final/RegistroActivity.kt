package com.academiasmoviles.proyecto_final

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.academiasmoviles.proyecto_final.databinding.ActivityRegistroBinding

class RegistroActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegistroBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //Action Bar
        val actionbar = supportActionBar
        actionbar!!.title = "Regístrate"
        actionbar.setDisplayHomeAsUpEnabled(false)

        binding.tvIngresaCuenta.setOnClickListener {
            startActivity(Intent(baseContext,LoginActivity::class.java))
        }

        binding.btnRegistrar.setOnClickListener {
            if (binding.edtUsuarioReg.text.toString().isNotEmpty()&&binding.edtClaveReg.text.toString().isNotEmpty()) {
                startActivity(Intent(baseContext, ListaActivity::class.java))
                Toast.makeText(baseContext, "Usuario Registrado", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(baseContext,"Debe ingresar un Usuario y Clave", Toast.LENGTH_SHORT).show()
            }

        }
    }

}