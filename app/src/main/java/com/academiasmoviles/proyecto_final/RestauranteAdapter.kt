package com.academiasmoviles.proyecto_final

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiasmoviles.proyecto_final.databinding.ItemRestauranteBinding
import com.academiasmoviles.proyecto_final.model.restaurante

class RestauranteAdapter (var restaurantes: MutableList<restaurante> = mutableListOf()) : RecyclerView.Adapter<RestauranteAdapter.RestauranteAdpaterViewHolder>() {
    lateinit var callbackAgregar:(restaurante)->Unit
    lateinit var callbackEditar:(restaurante)->Unit


    inner class RestauranteAdpaterViewHolder (itemview: View) : RecyclerView.ViewHolder(itemview){
        private val binding: ItemRestauranteBinding= ItemRestauranteBinding.bind(itemView)
        fun bind(restaurante: restaurante){
            binding.tvNombre.text = restaurante.nombre
            binding.tvTipo.text = restaurante.tipo
            binding.tvAtencion.text = restaurante.atencion
            binding.btnReservarr.setOnClickListener {
                callbackAgregar(restaurante)
            }
            binding.btnReservarr.setOnClickListener {
                callbackEditar(restaurante)
            }

        }
    }

    fun updateList(restaurantes: MutableList<restaurante>){
        this.restaurantes= restaurantes
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestauranteAdpaterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_restaurante,parent,false)
        return RestauranteAdpaterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return restaurantes.size
    }

    override fun onBindViewHolder(holder: RestauranteAdpaterViewHolder, position: Int) {
        val restaurantes = restaurantes[position]
        holder.bind(restaurantes)
    }
}