package com.academiasmoviles.proyecto_final.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.academiasmoviles.proyecto_final.model.restaurante

@Database(entities = [restaurante::class],version = 1,exportSchema = false)
abstract class AppDataBase: RoomDatabase() {

    abstract fun restauranteDao(): RestauranteDao
    companion object{

        private var instancia :AppDataBase?= null

        fun obtenerInstancia(context: Context): AppDataBase{
            if (instancia== null){
                //Crearla
                instancia= Room.databaseBuilder(
                    context,
                    AppDataBase::class.java,
                    "bdRestaurante").build()
            }
            return instancia as AppDataBase
        }

    }



}