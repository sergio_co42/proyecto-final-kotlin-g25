package com.academiasmoviles.proyecto_final.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.academiasmoviles.proyecto_final.model.restaurante
//Metodos para acceder a la bd
@Dao
interface RestauranteDao {
    @Insert
    fun insertar(restaurante: restaurante)

    @Update
    fun update(restaurante: restaurante)

    @Query("select * from tablaRestaurante order by cod")
    fun obtenerRestaurante(): LiveData<MutableList<restaurante>>
}