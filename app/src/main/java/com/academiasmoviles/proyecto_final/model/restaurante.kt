package com.academiasmoviles.proyecto_final.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "tablaRestaurante")
class restaurante (
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "cod")
    val codigo:Int,
    @ColumnInfo(name = "nombre")
    val nombre:String,
    @ColumnInfo(name = "tipo")
    val tipo:String,
    @ColumnInfo(name = "atencion")
    val atencion:String,
    ) : Serializable{

    }
